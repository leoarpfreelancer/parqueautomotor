package parqueautomotor.logica.parque;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

import parqueautomotor.logica.vehiculoPasajero.Auto0Km;

public class ParqueAutomotorTest {
    @Test
    public void testComprobarDisponibilidad() {

    }

    @Test
    public void testGetListadoVehiculos() {

    }

    @Test
    public void testListarVehiculosDisponibles() {

    }

    @Test
    public void testPresupuestarAlquilerCarga() {

    }

    @Test
    public void testPresupuestarAlquilerPasajero() {

    }

    @Test
    public void testPresupuestarVentaAuto() {
        ParqueAutomotor parque = new ParqueAutomotor();
        Auto0Km autonuevo = new Auto0Km("dada", "dudu", 100);

        try {
            parque.registrarVehiculo(autonuevo);
            Double valor = parque.presupuestarVentaAuto(autonuevo);

            assertEquals(Double.valueOf(150), valor);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testRecibirDevolucionVehiculo() {

    }

    @Test
    public void testRecuperarListadoAlquiler() {

    }

    @Test
    public void testRecuperarListadoAlquilerDeCarga() {

    }

    @Test
    public void testRecuperarListadoAlquilerDePasajeros() {

    }

    @Test
    public void testRecuperarListadoVenta() {

    }

    @Test
    public void testRegistrarVehiculo() {

    }

    @Test
    public void testReservarVehiculo() {

    }

    @Test
    public void testVender() {
        ParqueAutomotor parque = new ParqueAutomotor();
        Auto0Km autonuevo = new Auto0Km("dada", "dudu", 100);

        try {
            parque.registrarVehiculo(autonuevo);
            parque.vender(autonuevo);
            assertFalse(autonuevo.isDisponibilidad());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
