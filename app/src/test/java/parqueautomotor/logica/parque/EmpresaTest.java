package parqueautomotor.logica.parque;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.Test;

import parqueautomotor.logica.cliente.*;
import parqueautomotor.logica.vehiculoPasajero.Auto0Km;
import parqueautomotor.logica.parque.registroOperaciones.RegistroVentas;;

public class EmpresaTest {
    @Test
    public void testCompletarAlquiler() {

    }

    @Test
    public void testCotizarCostoAlquilerPasajero() {

    }

    @Test
    public void testCotizarCuotasAlquiler() {

    }

    @Test
    public void testDevolucionVehiculo() {

    }

    @Test
    public void testGetClientes() {

    }

    @Test
    public void testGetInstance() {

    }

    @Test
    public void testGetParque() {

    }

    @Test
    public void testGetPunto() {

    }

    @Test
    public void testGetReservas() {

    }

    @Test
    public void testGetVentas() {

    }

    @Test
    public void testObtenerPrecioFinalAlquiler() {

    }

    @SuppressWarnings("unused")
    @Test
    public void testRealizarVenta() {
        Empresa empresa = Empresa.getInstance();
        ParqueAutomotor parque = Empresa.getInstance().getParque();
        RegistroCliente clientes = Empresa.getInstance().getClientes();
        RegistroVentas ventas = Empresa.getInstance().getVentas();
        LocalDate hoy = LocalDate.now();

        Auto0Km autonuevo = new Auto0Km("dada", "dudu", 100);
        Cliente per = new Cliente("da", "we", "222", hoy, "as", "222", new Visa());

        try {
            parque.registrarVehiculo(autonuevo);
            clientes.registrarCliente(per);
            empresa.realizarVenta(per, autonuevo, hoy);

            boolean disponible = parque.comprobarDisponibilidad(autonuevo);

            assertFalse(disponible);
            parque.recibirDevolucionVehiculo(autonuevo);
            disponible = parque.comprobarDisponibilidad(autonuevo);
            assertTrue(disponible);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testReservarVehiculo() {

    }

    @Test
    public void testSolicitarCaracteristicaAdicional() {

    }
}
