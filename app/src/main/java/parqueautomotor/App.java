
package parqueautomotor;

import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import parqueautomotor.gui.Principal;
import parqueautomotor.logica.cliente.*;
import parqueautomotor.logica.excepciones.*;
import parqueautomotor.logica.parque.Empresa;
import parqueautomotor.logica.vehiculoPasajero.*;
import parqueautomotor.logica.vehiculoPasajero.componentes.*;


public class App {
    private static Empresa baseDeDatos = Empresa.getInstance();

    public static void main(String[] args) {
        Principal panelPrincipal  = new Principal(baseDeDatos);
        panelPrincipal.setVisible(true);
        panelPrincipal.setLocationRelativeTo(null);
        panelPrincipal.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        
        
        
        Auto0Km autonuevo = new Auto0Km("NISSAN", "1", 2000);
        autonuevo.agregarComponente(new Alarma());
        autonuevo.agregarComponente(new LevantaCristalElectrico());
        AutoUsado autousado = new AutoUsado("FIAT", "2", 500);
        Cliente cliente = new Cliente("DIAZ", "LEONEL", "123", LocalDate.of(1993, 12, 13) , "gmail", "38344444", new Visa());
        
        try{
            baseDeDatos.getClientes().registrarCliente(cliente);
        } catch (ClienteExisteException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            baseDeDatos.getParque().registrarVehiculo(autonuevo);
            baseDeDatos.getParque().registrarVehiculo(autousado);
        } catch (VehiculoExistenteException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
