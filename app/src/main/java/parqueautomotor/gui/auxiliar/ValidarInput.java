
package parqueautomotor.gui.auxiliar;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author AlexDev
 */
public class ValidarInput extends InputVerifier{

    @Override
    public boolean verify(JComponent input) {
        String texto = ((JTextField)input).getText();
        if (texto.isEmpty()) {
            return true;
        }
        for(char c : texto.toCharArray()){
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }
    
}
