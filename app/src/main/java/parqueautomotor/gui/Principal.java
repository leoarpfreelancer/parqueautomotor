package parqueautomotor.gui;


import javax.swing.ImageIcon;
import parqueautomotor.gui.menuBuscar.*;
import parqueautomotor.gui.menuBorrar.*;
import parqueautomotor.gui.menuListar.*;
import parqueautomotor.gui.menuRegistrar.*;
import parqueautomotor.gui.ventanaPrinciapal.*;
import parqueautomotor.logica.parque.Empresa;


/**
 *
 * @author AlexDev
 */
public class Principal extends javax.swing.JFrame {
    private Empresa baseDeDatos = Empresa.getInstance();
    
    
    public Principal(Empresa baseDeDatos){
        this.baseDeDatos = baseDeDatos;
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/images/ParkIcon32x32.png")).getImage());
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelPrincipal = new javax.swing.JPanel();
        btnNuevaVenta = new javax.swing.JButton();
        btnNuevaReserva = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuRegistrar = new javax.swing.JMenu();
        jMenuCargarVehiculoPasajero = new javax.swing.JMenu();
        jMenuCAutoNuevo = new javax.swing.JMenuItem();
        jMenuCAutoUsado = new javax.swing.JMenuItem();
        jMenuCMinibus = new javax.swing.JMenuItem();
        jMenuCargarVehiculoCarga = new javax.swing.JMenu();
        jMenuCCamioneta = new javax.swing.JMenuItem();
        jMenuCCamion = new javax.swing.JMenuItem();
        jMenuRCliente = new javax.swing.JMenuItem();
        jMenuBorrar = new javax.swing.JMenu();
        jMenuBuscar = new javax.swing.JMenu();
        jMenuVer = new javax.swing.JMenu();
        jMenuListarCliente = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuListarTodos = new javax.swing.JMenuItem();
        jMenuListarDisponibles = new javax.swing.JMenuItem();
        jMenuListarReservas = new javax.swing.JMenuItem();
        jMenuListarVentas = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnNuevaVenta.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnNuevaVenta.setText("Nueva Venta");
        btnNuevaVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaVentaActionPerformed(evt);
            }
        });

        btnNuevaReserva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnNuevaReserva.setText("Nueva Reserva");
        btnNuevaReserva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevaReservaActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/iconPark 1700x900.jpg"))); // NOI18N

        javax.swing.GroupLayout jPanelPrincipalLayout = new javax.swing.GroupLayout(jPanelPrincipal);
        jPanelPrincipal.setLayout(jPanelPrincipalLayout);
        jPanelPrincipalLayout.setHorizontalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addContainerGap(177, Short.MAX_VALUE)
                .addComponent(btnNuevaVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(143, 143, 143)
                .addComponent(btnNuevaReserva)
                .addGap(172, 172, 172))
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanelPrincipalLayout.setVerticalGroup(
            jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 442, Short.MAX_VALUE)
                .addGap(31, 31, 31)
                .addGroup(jPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnNuevaVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNuevaReserva, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34))
        );

        jMenuBar.setForeground(new java.awt.Color(92, 113, 119));
        jMenuBar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuBar.setName(""); // NOI18N

        jMenuRegistrar.setText("Registrar");
        jMenuRegistrar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jMenuCargarVehiculoPasajero.setText("Vehiculo de Pasajero");
        jMenuCargarVehiculoPasajero.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jMenuCAutoNuevo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuCAutoNuevo.setText("Auto Nuevo");
        jMenuCAutoNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCAutoNuevoActionPerformed(evt);
            }
        });
        jMenuCargarVehiculoPasajero.add(jMenuCAutoNuevo);

        jMenuCAutoUsado.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuCAutoUsado.setText("Auto Usado");
        jMenuCAutoUsado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCAutoUsadoActionPerformed(evt);
            }
        });
        jMenuCargarVehiculoPasajero.add(jMenuCAutoUsado);

        jMenuCMinibus.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuCMinibus.setText("Minibus");
        jMenuCMinibus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCMinibusActionPerformed(evt);
            }
        });
        jMenuCargarVehiculoPasajero.add(jMenuCMinibus);

        jMenuRegistrar.add(jMenuCargarVehiculoPasajero);

        jMenuCargarVehiculoCarga.setText("Vehiculo de Carga");
        jMenuCargarVehiculoCarga.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jMenuCCamioneta.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuCCamioneta.setText("Camioneta");
        jMenuCCamioneta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCCamionetaActionPerformed(evt);
            }
        });
        jMenuCargarVehiculoCarga.add(jMenuCCamioneta);

        jMenuCCamion.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuCCamion.setText("Camion");
        jMenuCCamion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCCamionActionPerformed(evt);
            }
        });
        jMenuCargarVehiculoCarga.add(jMenuCCamion);

        jMenuRegistrar.add(jMenuCargarVehiculoCarga);

        jMenuRCliente.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuRCliente.setText("Cliente");
        jMenuRCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuRClienteActionPerformed(evt);
            }
        });
        jMenuRegistrar.add(jMenuRCliente);

        jMenuBar.add(jMenuRegistrar);

        jMenuBorrar.setText("Borrar");
        jMenuBorrar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuBorrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuBorrarMouseClicked(evt);
            }
        });
        jMenuBar.add(jMenuBorrar);

        jMenuBuscar.setText("Buscar");
        jMenuBuscar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuBuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuBuscarMouseClicked(evt);
            }
        });
        jMenuBar.add(jMenuBuscar);

        jMenuVer.setText("Listar");
        jMenuVer.setFont(new java.awt.Font("Dialog", 0, 16)); // NOI18N

        jMenuListarCliente.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuListarCliente.setText("Clientes");
        jMenuListarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuListarClienteActionPerformed(evt);
            }
        });
        jMenuVer.add(jMenuListarCliente);

        jMenu1.setText("Vehiculo");
        jMenu1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jMenuListarTodos.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuListarTodos.setText("Todos");
        jMenuListarTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuListarTodosActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuListarTodos);

        jMenuListarDisponibles.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuListarDisponibles.setText("Disponibles");
        jMenuListarDisponibles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuListarDisponiblesActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuListarDisponibles);

        jMenuVer.add(jMenu1);

        jMenuListarReservas.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuListarReservas.setText("Reservas");
        jMenuListarReservas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuListarReservasActionPerformed(evt);
            }
        });
        jMenuVer.add(jMenuListarReservas);

        jMenuListarVentas.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jMenuListarVentas.setText("Ventas");
        jMenuListarVentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuListarVentasActionPerformed(evt);
            }
        });
        jMenuVer.add(jMenuListarVentas);

        jMenuBar.add(jMenuVer);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuListarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuListarClienteActionPerformed
        MenuTablaCliente clientes = new MenuTablaCliente(baseDeDatos);
        clientes.pack();
        clientes.setVisible(true);
        clientes.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuListarClienteActionPerformed

    private void btnNuevaReservaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaReservaActionPerformed
        NuevaReserva reservar = new NuevaReserva(baseDeDatos);
        reservar.setVisible(true);
        reservar.setLocationRelativeTo(null);
    }//GEN-LAST:event_btnNuevaReservaActionPerformed

    private void jMenuCAutoNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCAutoNuevoActionPerformed
        RegistrarAuto0Km auto = new RegistrarAuto0Km(baseDeDatos);
        auto.setVisible(true);
        auto.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuCAutoNuevoActionPerformed

    private void jMenuListarDisponiblesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuListarDisponiblesActionPerformed
        MenuTablaVehiculoDisponibles ventana = new MenuTablaVehiculoDisponibles(baseDeDatos);
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuListarDisponiblesActionPerformed

    private void jMenuCAutoUsadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCAutoUsadoActionPerformed
        RegistrarAutoUsado auto = new RegistrarAutoUsado(baseDeDatos);
        auto.setVisible(true);
        auto.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuCAutoUsadoActionPerformed

    private void jMenuCMinibusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCMinibusActionPerformed
        RegistrarMinibus auto = new RegistrarMinibus(baseDeDatos);
        auto.setVisible(true);
        auto.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuCMinibusActionPerformed

    private void jMenuCCamionetaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCCamionetaActionPerformed
        RegistrarCamioneta auto = new RegistrarCamioneta(baseDeDatos);
        auto.setVisible(true);
        auto.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuCCamionetaActionPerformed

    private void jMenuCCamionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCCamionActionPerformed
        RegistrarCamion auto = new RegistrarCamion(baseDeDatos);
        auto.setVisible(true);
        auto.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuCCamionActionPerformed

    private void jMenuRClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuRClienteActionPerformed
        RegistrarCliente cliente = new RegistrarCliente(baseDeDatos);
        cliente.setVisible(true);
        cliente.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuRClienteActionPerformed

    private void jMenuListarTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuListarTodosActionPerformed
        MenuTablaVehiculo vehiculos = new MenuTablaVehiculo(baseDeDatos);
        vehiculos.pack();
        vehiculos.setVisible(true);
        vehiculos.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuListarTodosActionPerformed

    private void btnNuevaVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevaVentaActionPerformed
        NuevaVenta vender = new NuevaVenta(baseDeDatos);
        vender.setVisible(true);
        vender.setLocationRelativeTo(null);
    }//GEN-LAST:event_btnNuevaVentaActionPerformed

    private void jMenuListarReservasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuListarReservasActionPerformed
        MenuTablaReserva reservas = new MenuTablaReserva(baseDeDatos);
        reservas.pack();
        reservas.setVisible(true);
        reservas.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuListarReservasActionPerformed

    private void jMenuListarVentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuListarVentasActionPerformed
        MenuTablaVenta ventas = new MenuTablaVenta(baseDeDatos);
        ventas.setVisible(true);
        ventas.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuListarVentasActionPerformed

    private void jMenuBuscarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuBuscarMouseClicked
        MenuBuscar buscar = new MenuBuscar(baseDeDatos);
        buscar.setVisible(true);
        buscar.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuBuscarMouseClicked

    private void jMenuBorrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuBorrarMouseClicked
        MenuEliminar ventana = new MenuEliminar(baseDeDatos);
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(null);
    }//GEN-LAST:event_jMenuBorrarMouseClicked

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnNuevaReserva;
    private javax.swing.JButton btnNuevaVenta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuBorrar;
    private javax.swing.JMenu jMenuBuscar;
    private javax.swing.JMenuItem jMenuCAutoNuevo;
    private javax.swing.JMenuItem jMenuCAutoUsado;
    private javax.swing.JMenuItem jMenuCCamion;
    private javax.swing.JMenuItem jMenuCCamioneta;
    private javax.swing.JMenuItem jMenuCMinibus;
    private javax.swing.JMenu jMenuCargarVehiculoCarga;
    private javax.swing.JMenu jMenuCargarVehiculoPasajero;
    private javax.swing.JMenuItem jMenuListarCliente;
    private javax.swing.JMenuItem jMenuListarDisponibles;
    private javax.swing.JMenuItem jMenuListarReservas;
    private javax.swing.JMenuItem jMenuListarTodos;
    private javax.swing.JMenuItem jMenuListarVentas;
    private javax.swing.JMenuItem jMenuRCliente;
    private javax.swing.JMenu jMenuRegistrar;
    private javax.swing.JMenu jMenuVer;
    private javax.swing.JPanel jPanelPrincipal;
    // End of variables declaration//GEN-END:variables
}
