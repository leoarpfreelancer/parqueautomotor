
package parqueautomotor.gui.menuBuscar;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.excepciones.ClienteNoEncontradoException;
import parqueautomotor.logica.excepciones.VehiculoInexistenteException;
import parqueautomotor.logica.vehiculoCarga.Camion;
import parqueautomotor.logica.vehiculoCarga.Camioneta;
import parqueautomotor.logica.vehiculoCarga.VehiculoDeCarga;
import parqueautomotor.logica.vehiculoPasajero.Auto0Km;
import parqueautomotor.logica.vehiculoPasajero.AutoUsado;
import parqueautomotor.logica.vehiculoPasajero.Minibus;
import parqueautomotor.logica.parque.Empresa;
import parqueautomotor.logica.parque.Vehiculo;



/**
 *
 * @author AlexDev
 */
public class MenuBuscar extends javax.swing.JFrame {
    private Empresa baseDeDatos;
    /**
     * Creates new form menuBuscar
     */
    public MenuBuscar(Empresa baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        txtBuscador.setEnabled(false);
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        btnBuscar = new javax.swing.JButton();
        txtBuscador = new javax.swing.JTextField();
        cmbBuscador = new javax.swing.JComboBox<>();
        lblBuscar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel6.setText("BUSCADOR");

        btnBuscar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnBuscar.setText("BUSCAR");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        txtBuscador.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        txtBuscador.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        cmbBuscador.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cmbBuscador.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Vehiculo", "Cliente" }));
        cmbBuscador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbBuscadorActionPerformed(evt);
            }
        });

        lblBuscar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        lblBuscar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblBuscar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBuscador, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(151, 151, 151)
                        .addComponent(jLabel6))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(141, 141, 141)
                        .addComponent(cmbBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(116, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jLabel6)
                .addGap(88, 88, 88)
                .addComponent(cmbBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txtBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(btnBuscar)
                .addGap(72, 72, 72))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbBuscadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbBuscadorActionPerformed
        if(cmbBuscador.getSelectedIndex() == 0){
            txtBuscador.setEnabled(false);
        }
        if (cmbBuscador.getSelectedIndex()==1) {
            lblBuscar.setText("Ingrese Patente");
            txtBuscador.setEnabled(true);
        }
        if (cmbBuscador.getSelectedIndex()==2) {
            lblBuscar.setText("Ingrese DNI");
            txtBuscador.setEnabled(true);
        }
    }//GEN-LAST:event_cmbBuscadorActionPerformed

    @SuppressWarnings("null")
    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        String clave;
        JFrame ventana = null;
        if(cmbBuscador.getSelectedIndex() == 0){
            btnBuscar.setEnabled(false);
        }
        if (cmbBuscador.getSelectedIndex()==1) {
            clave = txtBuscador.getText();
            try{
                Vehiculo buscado = baseDeDatos.getParque().consultarVehiculo(clave);
                if(buscado instanceof Camion || buscado instanceof Camioneta){
                    VehiculoDeCarga encontrado = (VehiculoDeCarga)buscado;
                    ventana = new MostrarCarga(encontrado);
                }
                if(buscado instanceof Auto0Km){
                    Auto0Km encontrado = (Auto0Km)buscado;
                    ventana = new MostrarAuto0km(encontrado);
                }
                if(buscado instanceof AutoUsado){
                    AutoUsado encontrado = (AutoUsado)buscado;
                    ventana = new MostrarAutoUsado(encontrado);
                }
                if(buscado instanceof Minibus){
                    Minibus encontrado = (Minibus)buscado;
                    ventana = new MostrarMinibus(encontrado);
                }
                
                ventana.setVisible(true);
                ventana.setLocationRelativeTo(null);
            } catch (VehiculoInexistenteException e){
                JOptionPane.showMessageDialog(this, e.toString(), "No hay coincidencia", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
        }
        if (cmbBuscador.getSelectedIndex()==2) {
            clave = txtBuscador.getText();
            try{
                Cliente buscado = baseDeDatos.getClientes().verFichaCliente(clave);
                ventana = new MostrarCliente(buscado);
                ventana.setVisible(true);
                ventana.setLocationRelativeTo(null);
            }catch (ClienteNoEncontradoException e){
                JOptionPane.showMessageDialog(this, e.toString(), "No hay coincidencia", JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JComboBox<String> cmbBuscador;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblBuscar;
    private javax.swing.JTextField txtBuscador;
    // End of variables declaration//GEN-END:variables
}
