
package parqueautomotor.gui.menuRegistrar;

import javax.swing.JOptionPane;

import parqueautomotor.gui.auxiliar.ValidarInput;
import parqueautomotor.logica.excepciones.VehiculoExistenteException;
import parqueautomotor.logica.vehiculoPasajero.Auto0Km;
import parqueautomotor.logica.vehiculoPasajero.componentes.AireAcondicionado;
import parqueautomotor.logica.vehiculoPasajero.componentes.Alarma;
import parqueautomotor.logica.vehiculoPasajero.componentes.LevantaCristalElectrico;
import parqueautomotor.logica.parque.Empresa;

/**
 *
 * @author AlexDev
 */
public class RegistrarAuto0Km extends javax.swing.JFrame {
    private Empresa baseDeDatos;
    /**
     * Creates new form RegistrarAutoNuevo
     */
    public RegistrarAuto0Km(Empresa baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
    }

    
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtMarca = new javax.swing.JTextField();
        txtPatente = new javax.swing.JTextField();
        txtPrecioBase = new javax.swing.JTextField();
        cbAlarma = new javax.swing.JCheckBox();
        cbAireAcondicionado = new javax.swing.JCheckBox();
        cbLevantaCristal = new javax.swing.JCheckBox();
        btnAceptar = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel1.setText("REGISTRAR AUTO 0KM");

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("Marca");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("Patente");

        jLabel4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel4.setText("Precio base");

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel5.setText("Componentes");

        txtMarca.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        // txtMarca.addActionListener(new java.awt.event.ActionListener() {
        //     public void actionPerformed(java.awt.event.ActionEvent evt) {
        //         txtMarcaActionPerformed(evt);
        //     }
        // });

        txtPatente.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        // txtPatente.addActionListener(new java.awt.event.ActionListener() {
        //     public void actionPerformed(java.awt.event.ActionEvent evt) {
        //         txtPatenteActionPerformed(evt);
        //     }
        // });

        txtPrecioBase.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        // txtPrecioBase.addActionListener(new java.awt.event.ActionListener() {
        //     public void actionPerformed(java.awt.event.ActionEvent evt) {
        //         txtPrecioBaseActionPerformed(evt);
        //     }
        // });

        cbAlarma.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cbAlarma.setText("Alarma");
        // cbAlarma.addActionListener(new java.awt.event.ActionListener() {
        //     public void actionPerformed(java.awt.event.ActionEvent evt) {
        //         cbAlarmaActionPerformed(evt);
        //     }
        // });

        cbAireAcondicionado.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cbAireAcondicionado.setText("Aire Acondicionado");
        // cbAireAcondicionado.addActionListener(new java.awt.event.ActionListener() {
        //     public void actionPerformed(java.awt.event.ActionEvent evt) {
        //         cbAireAcondicionadoActionPerformed(evt);
        //     }
        // });

        cbLevantaCristal.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cbLevantaCristal.setText("Levanta Cristal Electrico");
        // cbLevantaCristal.addActionListener(new java.awt.event.ActionListener() {
        //     public void actionPerformed(java.awt.event.ActionEvent evt) {
        //         cbLevantaCristalActionPerformed(evt);
        //     }
        // });

        btnAceptar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnAceptar.setText("ACEPTAR");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnCancel.setText("CANCELAR");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnAceptar)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3)
                                .addComponent(jLabel4)
                                .addComponent(jLabel5)))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(64, 64, 64)
                                .addComponent(btnCancel))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbAlarma)
                                    .addComponent(cbAireAcondicionado)
                                    .addComponent(cbLevantaCristal)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtPrecioBase, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtPatente, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtMarca, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(73, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(jLabel1)
                .addGap(51, 51, 51)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPatente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPrecioBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbAlarma)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbAireAcondicionado)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbLevantaCristal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancel))
                .addGap(63, 63, 63))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // private void txtMarcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMarcaActionPerformed
    // }//GEN-LAST:event_txtMarcaActionPerformed

    // private void txtPatenteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPatenteActionPerformed
    // }//GEN-LAST:event_txtPatenteActionPerformed

    // private void txtPrecioBaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrecioBaseActionPerformed
    // }//GEN-LAST:event_txtPrecioBaseActionPerformed

    // private void cbAlarmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAlarmaActionPerformed
    // }//GEN-LAST:event_cbAlarmaActionPerformed

    // private void cbAireAcondicionadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAireAcondicionadoActionPerformed
    // }//GEN-LAST:event_cbAireAcondicionadoActionPerformed

    // private void cbLevantaCristalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbLevantaCristalActionPerformed
    // }//GEN-LAST:event_cbLevantaCristalActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        String marca, patente;
        boolean checkAlarma, checkAireAcondicionado, checkLevantaCristal;
        double precioBase;
        checkAlarma = cbAlarma.isSelected();
        checkAireAcondicionado = cbAireAcondicionado.isSelected();
        checkLevantaCristal = cbLevantaCristal.isSelected();
        marca = txtMarca.getText();
        patente = txtPatente.getText();
        
        try{
            txtPrecioBase.setInputVerifier(new ValidarInput());
            precioBase = Integer.parseInt(txtPrecioBase.getText());
        } catch (NumberFormatException e){
            JOptionPane.showMessageDialog(this, e.toString(), "El precio debe ser caracter numerico", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        
        try{
            Auto0Km auto = new Auto0Km(marca, patente, precioBase);
            if(checkAlarma){
            Alarma alarma = new Alarma();
            auto.agregarComponente(alarma);
            }
            if(checkAireAcondicionado){
            AireAcondicionado aire = new AireAcondicionado();
            auto.agregarComponente(aire);
            }
            if(checkLevantaCristal){
            LevantaCristalElectrico levanta = new LevantaCristalElectrico();
            auto.agregarComponente(levanta);
            }
            baseDeDatos.getParque().registrarVehiculo(auto);
            JOptionPane.showMessageDialog(this, "Vehiculo Registrado");
            dispose();
            
        } catch(VehiculoExistenteException e){
            JOptionPane.showMessageDialog(this, e.toString(), "Esta patente ya esta registrada", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancel;
    private javax.swing.JCheckBox cbAireAcondicionado;
    private javax.swing.JCheckBox cbAlarma;
    private javax.swing.JCheckBox cbLevantaCristal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtPatente;
    private javax.swing.JTextField txtPrecioBase;
    // End of variables declaration//GEN-END:variables
}
