
package parqueautomotor.gui.ventanaPrinciapal;

import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.excepciones.ClienteNoEncontradoException;
import parqueautomotor.logica.excepciones.ParametrosIncorrectosException;
import parqueautomotor.logica.excepciones.VehiculoInexistenteException;
import parqueautomotor.logica.vehiculoPasajero.Auto0Km;
import parqueautomotor.logica.vehiculoPasajero.AutoUsado;
import parqueautomotor.logica.parque.Empresa;
import parqueautomotor.logica.parque.Vehiculo;

/**
 *
 * @author AlexDev
 */
public class NuevaVenta extends javax.swing.JFrame {
    private Empresa baseDeDatos;
    /**
     * Creates new form NuevaVenta
     * @param baseDeDatos
     */
    public NuevaVenta(Empresa baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
        mostrarComboClientes();
        mostrarComboTipoVehiculo();
        mostrarComboVehiculos();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        txtPrecioVenta.setEditable(false);
        if(cmbVehiculo.getSelectedIndex()!=0){        
            mostrarPrecio();
        }
        cmbVehiculoTipo.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrarComboVehiculos();
            }
        });
        cmbVehiculo.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrarPrecio();
            }
        });
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnAceptar = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cmbVehiculoTipo = new javax.swing.JComboBox<>();
        cmbClientes = new javax.swing.JComboBox<>();
        btnCancel = new javax.swing.JButton();
        txtPrecioVenta = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        cmbVehiculo = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel6.setText("Nueva Venta");

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel7.setText("Tipo");

        btnAceptar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnAceptar.setText("ACEPTAR");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel8.setText("Cliente");

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setText("Precio Venta");

        cmbVehiculoTipo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        cmbClientes.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnCancel.setText("CANCELAR");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        txtPrecioVenta.setEditable(false);
        txtPrecioVenta.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("Vehiculo");

        cmbVehiculo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(232, 232, 232)
                        .addComponent(jLabel6))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(151, 151, 151)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1))
                                .addGap(39, 39, 39)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtPrecioVenta, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                                    .addComponent(cmbVehiculoTipo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cmbClientes, 0, 128, Short.MAX_VALUE)
                                    .addComponent(cmbVehiculo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap(176, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabel6)
                .addGap(32, 32, 32)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cmbVehiculoTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbVehiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(cmbClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txtPrecioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(71, 71, 71)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancel))
                .addGap(66, 66, 66))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 605, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 442, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        String selectedVehiculo = (String) cmbVehiculo.getSelectedItem();
        String selectedCliente = (String) cmbClientes.getSelectedItem();
        LocalDate fecha = LocalDate.now();
        
        String[] partesC = selectedCliente.split(" ");
        String dni = partesC[1];
        Cliente cliente=null;
        
        String[] partesV = selectedVehiculo.split(" ");
        String patente = partesV[1];
        Vehiculo movil=null;
        
        try{
            cliente = baseDeDatos.getClientes().recuperarCliente(dni);
        } catch (ClienteNoEncontradoException ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "No hay coincidencia Cliente", JOptionPane.ERROR_MESSAGE);
        }

        try{
            movil = baseDeDatos.getParque().consultarVehiculo(patente);
        }catch (VehiculoInexistenteException ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "No hay coincidencia Vehiculo", JOptionPane.ERROR_MESSAGE);
        }
        
        try{
            if(cliente!=null && movil!=null){
            btnAceptar.setEnabled(true);
            baseDeDatos.realizarVenta(cliente, movil, fecha);
            JOptionPane.showMessageDialog(this, "Venta exitosa!");
            dispose();
            }        
        } catch (Exception ex) {
            Logger.getLogger(NuevaVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnAceptarActionPerformed

    private void mostrarPrecio(){
        String selectedVehiculo = (String) cmbVehiculo.getSelectedItem();
        if (selectedVehiculo != null && !selectedVehiculo.equals("Seleccione")) {
        String[] partesV = selectedVehiculo.split(" ");
        String patente = partesV[1];
        
        try{
            Vehiculo movil = baseDeDatos.getParque().consultarVehiculo(patente);
            double precio = baseDeDatos.getParque().presupuestarVentaAuto(movil);
            txtPrecioVenta.setText(String.valueOf(precio));
        } catch (VehiculoInexistenteException ex)  {
            JOptionPane.showMessageDialog(this, ex.toString(), "No hay precio", JOptionPane.ERROR_MESSAGE);
            } catch (ParametrosIncorrectosException ex){
                        JOptionPane.showMessageDialog(this, ex.toString(), "No hay precio 2", JOptionPane.ERROR_MESSAGE);
            }
        }else {
            txtPrecioVenta.setText("");
        }
    }
    
    
    private void mostrarComboTipoVehiculo(){
        List<Vehiculo> disponibles = baseDeDatos.getParque().recuperarListadoVenta();
        cmbVehiculoTipo.addItem("Seleccione");
        if(baseDeDatos.getParque().tieneObjeto(disponibles, AutoUsado.class)){
            cmbVehiculoTipo.addItem("AUTO USADO");
        }
        if(baseDeDatos.getParque().tieneObjeto(disponibles, Auto0Km.class)){
            cmbVehiculoTipo.addItem("AUTO 0KM");
        } 
    }
    
    private void mostrarComboVehiculos(){
        List<Vehiculo> vehiculos = null;
        cmbVehiculo.removeAllItems();
        cmbVehiculo.addItem("Seleccione");
        String selectedTipo = (String) cmbVehiculoTipo.getSelectedItem();
        if(null != selectedTipo)switch (selectedTipo) {
        case "Seleccione":
//            cmbVehiculo.setEnabled(false);
            break;
        case "AUTO USADO":
            vehiculos = baseDeDatos.getParque().listadoAutosUsados();
            agregarElementosAComboVehiculo(vehiculos);
//            cmbVehiculo.setEnabled(true);
            break;
        case "AUTO 0KM":
            vehiculos = baseDeDatos.getParque().listadoAutosNuevos();
            agregarElementosAComboVehiculo(vehiculos);
//            cmbVehiculo.setEnabled(true);
            break;
        default:
            cmbVehiculo.setEnabled(false);
            break;
    }
    }
    
    public void agregarElementosAComboVehiculo(List<Vehiculo> vehiculos){
        for(Vehiculo movil : vehiculos){
            cmbVehiculo.addItem(movil.getMarca() + " " + movil.getPatente());
        }
    }
    
    private void mostrarComboClientes(){
        List<Cliente> clientes = baseDeDatos.getClientes().getRegistroClientes();
        cmbClientes.addItem("Seleccione");
        for (Cliente cliente : clientes)
            cmbClientes.addItem(cliente.getApellido()+" " + cliente.getDni());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancel;
    private javax.swing.JComboBox<String> cmbClientes;
    private javax.swing.JComboBox<String> cmbVehiculo;
    private javax.swing.JComboBox<String> cmbVehiculoTipo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtPrecioVenta;
    // End of variables declaration//GEN-END:variables
}
