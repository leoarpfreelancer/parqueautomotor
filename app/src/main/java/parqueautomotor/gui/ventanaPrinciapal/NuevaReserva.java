
package parqueautomotor.gui.ventanaPrinciapal;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.excepciones.ClienteEdadInsuficienteException;
import parqueautomotor.logica.excepciones.ClienteNoEncontradoException;
import parqueautomotor.logica.excepciones.ParametrosIncorrectosException;
import parqueautomotor.logica.excepciones.VehiculoInexistenteException;
import parqueautomotor.logica.vehiculoCarga.*;
import parqueautomotor.logica.vehiculoPasajero.*;
import parqueautomotor.logica.parque.Empresa;
import parqueautomotor.logica.parque.Vehiculo;

/**
 *
 * @author AlexDev
 */
public class NuevaReserva extends javax.swing.JFrame {
    private Empresa baseDeDatos;
    /**
     * Creates new form NuevaReserva
     */
    public NuevaReserva(Empresa baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
        mostrarComboTipoVehiculo();
        mostrarComboClientes();
        cmbTipoVehiculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mostrarComboVehiculos();
            }
        });
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnAceptar = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        cmbTipoVehiculo = new javax.swing.JComboBox<>();
        cmbClientes = new javax.swing.JComboBox<>();
        cbSilla = new javax.swing.JCheckBox();
        cbGPS = new javax.swing.JCheckBox();
        cbConductor = new javax.swing.JCheckBox();
        btnCancel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbVehiculos = new javax.swing.JComboBox<>();
        cbSeguro = new javax.swing.JCheckBox();
        jLabel2 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel6.setText("NUEVA RESERVA");

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel7.setText("TIpo disponible");

        btnAceptar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnAceptar.setText("ACEPTAR");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel8.setText("Cliente");

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setText("Caracteristica Extras");

        cmbTipoVehiculo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cmbTipoVehiculo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione" }));

        cmbClientes.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cmbClientes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione" }));

        cbSilla.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cbSilla.setText("SILLA PARA BEBE");

        cbGPS.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cbGPS.setText("DISPOSITIVO GPS");

        cbConductor.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cbConductor.setText("CONDUCTOR EXTRA");

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnCancel.setText("CANCELAR");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("Vehiculo");

        cmbVehiculos.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        cbSeguro.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cbSeguro.setText("SEGURO CONTRA TERCEROS");

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("Fecha");

        txtFecha.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel7)
                    .addComponent(jLabel2)
                    .addComponent(jLabel8)
                    .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbGPS)
                            .addComponent(cbSilla)
                            .addComponent(cbSeguro)
                            .addComponent(cbConductor))))
                .addGap(40, 40, 40))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(72, 72, 72))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cmbTipoVehiculo, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbVehiculos, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(88, 88, 88))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(182, 182, 182)
                .addComponent(jLabel6)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jLabel6)
                .addGap(57, 57, 57)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cmbTipoVehiculo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbVehiculos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(cmbClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(cbSeguro)
                        .addGap(0, 0, 0)
                        .addComponent(cbGPS)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbConductor)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cbSilla))
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar)
                    .addComponent(btnCancel))
                .addGap(54, 54, 54))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        String selectedVehiculo = (String) cmbVehiculos.getSelectedItem();
        String selectedCliente = (String) cmbClientes.getSelectedItem();
        boolean seguro = cbSeguro.isSelected();
        boolean gps = cbGPS.isSelected();
        boolean silla = cbSilla.isSelected();
        boolean chofer = cbConductor.isSelected();
        String fecha = txtFecha.getText();
        LocalDate fechaReserva;
        
        String[] partesC = selectedCliente.split(" ");
        String dni = partesC[1];
        Cliente cliente=null;
        
        String[] partesV = selectedVehiculo.split(" ");
        String patente = partesV[1];
        Vehiculo movil=null;
        
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            fechaReserva = LocalDate.parse(fecha, formatter);

        } catch (DateTimeParseException e) {
            JOptionPane.showMessageDialog(this, e.toString(), "Formato de fecha erroneo", JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        try{
            cliente = baseDeDatos.getClientes().recuperarCliente(dni);
        } catch (ClienteNoEncontradoException ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "No hay coincidencia: Cliente", JOptionPane.ERROR_MESSAGE);
        }

        try{
            movil = baseDeDatos.getParque().consultarVehiculo(patente);
        }catch (VehiculoInexistenteException ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "No hay coincidencia: Vehiculo", JOptionPane.ERROR_MESSAGE);
        }
        
        try{
            if(cliente!=null && movil!=null){
            btnAceptar.setEnabled(true);
            baseDeDatos.reservarVehiculo(cliente, movil, fechaReserva, seguro, silla, gps, chofer);
            JOptionPane.showMessageDialog(this, "Reserva exitosa!");
            dispose();
            }
        } catch (ClienteEdadInsuficienteException e){
            JOptionPane.showMessageDialog(this, e.toString(), " mayor de 21 años ", JOptionPane.ERROR_MESSAGE);
            return;
        } catch (ParametrosIncorrectosException e){
            JOptionPane.showMessageDialog(this, e.toString(), "tarjeta de credito", JOptionPane.ERROR_MESSAGE);
            return;
        }        
        
    }//GEN-LAST:event_btnAceptarActionPerformed

    public void mostrarComboTipoVehiculo(){
        List<Vehiculo> disponibles = baseDeDatos.getParque().recuperarListadoAlquiler();
        if(baseDeDatos.getParque().tieneObjeto(disponibles, Camion.class)){
            cmbTipoVehiculo.addItem("CAMION");
        } 
        if(baseDeDatos.getParque().tieneObjeto(disponibles, Camioneta.class)){
            cmbTipoVehiculo.addItem("CAMIONETA");
        } 
        if(baseDeDatos.getParque().tieneObjeto(disponibles, AutoUsado.class)){
            cmbTipoVehiculo.addItem("AUTO");
        } 
        if(baseDeDatos.getParque().tieneObjeto(disponibles, Minibus.class)){
            cmbTipoVehiculo.addItem("MINIBUS");
        }
    }
    
    public void inhabilitarBoton(){
        String selectedTipo = (String) cmbTipoVehiculo.getSelectedItem();
        String selectedVehiculo = (String) cmbVehiculos.getSelectedItem();
        if("Seleccione".equals(selectedTipo)){
            cmbVehiculos.setEnabled(false);
        }else{
            cmbVehiculos.setEnabled(true);
        }
        if(!cmbVehiculos.isEnabled() || "Seleccione".equals(selectedVehiculo) ){
            btnAceptar.setEnabled(false);
        }else{
            btnAceptar.setEnabled(true);
        }        
    }
    
    public void mostrarComboVehiculos(){
        List<Vehiculo> vehiculos = null;
        cmbVehiculos.removeAllItems();
        cmbVehiculos.addItem("Seleccione");
        String selectedItem = (String) cmbTipoVehiculo.getSelectedItem();
        if(null != selectedItem)switch (selectedItem) {
            case "CAMION":
                vehiculos=baseDeDatos.getParque().listadoCamiones();
                agregarElementosAComboVehiculo(vehiculos);
                break;
            case "CAMIONETA":
                vehiculos=baseDeDatos.getParque().listadoCamionetas();
                agregarElementosAComboVehiculo(vehiculos);
                break;
            case "MINIBUS":
                vehiculos =baseDeDatos.getParque().listadoMinibuses();
                agregarElementosAComboVehiculo(vehiculos);
                break;
            case "AUTO":
                vehiculos=baseDeDatos.getParque().listadoAutosUsados();
                agregarElementosAComboVehiculo(vehiculos);
                break;
            default:
                break;
        }
        
    }
    
    public void agregarElementosAComboVehiculo(List<Vehiculo> vehiculos){
        if(vehiculos != null && !vehiculos.isEmpty()){
            for(Vehiculo movil : vehiculos){
                cmbVehiculos.addItem(movil.getMarca() + " " + movil.getPatente());
            }
        }
    }
    
    private void mostrarComboClientes(){
        List<Cliente> clientes = baseDeDatos.getClientes().getRegistroClientes();
        for (Cliente cliente : clientes)
            cmbClientes.addItem(cliente.getApellido()+" " + cliente.getDni());
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancel;
    private javax.swing.JCheckBox cbConductor;
    private javax.swing.JCheckBox cbGPS;
    private javax.swing.JCheckBox cbSeguro;
    private javax.swing.JCheckBox cbSilla;
    private javax.swing.JComboBox<String> cmbClientes;
    private javax.swing.JComboBox<String> cmbTipoVehiculo;
    private javax.swing.JComboBox<String> cmbVehiculos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtFecha;
    // End of variables declaration//GEN-END:variables
}
