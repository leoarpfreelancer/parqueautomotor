
package parqueautomotor.gui.menuListar;

import java.util.List;
import javax.swing.table.AbstractTableModel;

import parqueautomotor.logica.cliente.Cliente;

/**
 *
 * @author AlexDev
 */
public class ClientesTableModel extends AbstractTableModel{

    private List<Cliente> clientes;

    ClientesTableModel(List<Cliente> registroClientes) {
        this.clientes = registroClientes;
    }
    
    
    @Override
    public int getRowCount() {
        return clientes.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cliente cliente = clientes.get(rowIndex);

        // Obtiene el valor correspondiente a la fila y columna especificadas
        // (por ejemplo, nombre, apellido, edad)
        switch (columnIndex) {
            case 0:
                return cliente.getApellido();
            case 1:
                return cliente.getNombre();
            case 2:
                return cliente.getDni();
            case 3:
                return cliente.getTelefono();
            case 4:
                return cliente.getEmail();
            case 5:
                return cliente.edad();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        // Define los nombres de las columnas de tu tabla (por ejemplo, "Nombre", "Apellido", "Edad")
        switch (columnIndex) {
            case 0:
                return "Apellido";
            case 1:
                return "Nombre";
            case 2:
                return "DNI";
            case 3:
                return "Telefono";
            case 4:
                return "Email";
            case 5:
                return "Edad";
            default:
                return null;
        }
    }
    
    
    
}
