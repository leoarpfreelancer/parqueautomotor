
package parqueautomotor.gui.menuListar;

import java.util.List;
import javax.swing.table.AbstractTableModel;

import parqueautomotor.logica.parque.Vehiculo;

/**
 *
 * @author AlexDev
 */
public class VehiculoTableModel extends AbstractTableModel{
     private List<Vehiculo> vehiculos;

    public VehiculoTableModel(List<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }
    
    
    @Override
    public int getRowCount() {
        return vehiculos.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Vehiculo movil = vehiculos.get(rowIndex);

        // Obtiene el valor correspondiente a la fila y columna especificadas
        // (por ejemplo, nombre, apellido, edad)
        switch (columnIndex) {
            case 0:
                return movil.getMarca();
            case 1:
                return movil.getPatente();
            case 2:
                return movil.isDisponibilidad();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        // Define los nombres de las columnas de tu tabla (por ejemplo, "Nombre", "Apellido", "Edad")
        switch (columnIndex) {
            case 0:
                return "Marca";
            case 1:
                return "Patente";
            case 2:
                return "Disponible";
            default:
                return null;
        }
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex >= 2 && columnIndex <= 3) {
            return Boolean.class; 
        } else {
            return super.getColumnClass(columnIndex);
        }
    }
}
