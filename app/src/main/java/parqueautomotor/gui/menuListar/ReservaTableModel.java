
package parqueautomotor.gui.menuListar;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import parqueautomotor.logica.parque.registroOperaciones.Reserva;

/**
 *
 * @author AlexDev
 */
public class ReservaTableModel extends AbstractTableModel{
    private List<Reserva> reservas;

    ReservaTableModel(List<Reserva> reservas) {
        this.reservas = reservas;
    }
    
    
    @Override
    public int getRowCount() {
        return reservas.size();
    }

    @Override
    public int getColumnCount() {
        return 8;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Reserva reserva = reservas.get(rowIndex);

        
        switch (columnIndex) {
            case 0:
                return reserva.getCliente();
            case 1:
                return reserva.getVehiculo();
            case 2:
                return reserva.getFechaDeReserva();
            case 3:
                return reserva.saberSiTieneSeguro();
            case 4:
                return reserva.saberSiTieneSilla();
            case 5:
                return reserva.saberSiTieneGPS();
            case 6:
                return reserva.saberSiTieneChofer();
            case 7:
                return reserva.getEstado();
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Cliente";
            case 1:
                return "Vehiculo";
            case 2:
                return "Fecha de Reserva";
            case 3:
                return "Seguro";
            case 4:
                return "Silla p/ bebe";
            case 5:
                return "Disp GPS";
            case 6:
                return "Conductor";
            case 7:
                return "Vigente";
            default:
                return null;
        }
    }
    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex >= 3 && columnIndex <= 7) {
            return Boolean.class; 
        } else {
            return super.getColumnClass(columnIndex);
        }
    }
}
