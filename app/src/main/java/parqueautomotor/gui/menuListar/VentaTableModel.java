
package parqueautomotor.gui.menuListar;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import parqueautomotor.logica.parque.registroOperaciones.Vendido;

/**
 *
 * @author AlexDev
 */
public class VentaTableModel extends AbstractTableModel {
    private List<Vendido> ventas;

    VentaTableModel(List<Vendido> ventas) {
        this.ventas = ventas;
    }
    
    
    @Override
    public int getRowCount() {
        return ventas.size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Vendido venta = ventas.get(rowIndex);

        
        switch (columnIndex) {
            case 0:
                return venta.getAuto();
            case 1:
                return venta.getComprador();
            case 2:
                return venta.getFechaCompra();
            case 3:
                return venta.getPreciofinal();            
            default:
                return null;
        }
    }
    
    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Vehiculo";
            case 1:
                return "Cliente";
            case 2:
                return "Fecha de Venta";
            case 3:
                return "Precio de Venta";            
            default:
                return null;
        }
    }
    
}
