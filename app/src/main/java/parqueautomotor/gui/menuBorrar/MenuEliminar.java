
package parqueautomotor.gui.menuBorrar;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.excepciones.ClienteNoEncontradoException;
import parqueautomotor.logica.excepciones.VehiculoInexistenteException;
import parqueautomotor.logica.parque.Empresa;
import parqueautomotor.logica.parque.Vehiculo;

/**
 *
 * @author AlexDev
 */
public class MenuEliminar extends javax.swing.JFrame {
    private Empresa baseDeDatos;
    /**
     * Creates new form EliminarVehiculo
     */
    public MenuEliminar(Empresa baseDeDatos) {
        this.baseDeDatos = baseDeDatos;
        initComponents();
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        lblBuscar = new javax.swing.JLabel();
        btnMostrar = new javax.swing.JButton();
        txtBuscado = new javax.swing.JTextField();
        btnCancel = new javax.swing.JButton();
        cmbBuscador = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel6.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        jLabel6.setText("DAR DE BAJA");

        lblBuscar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        btnMostrar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnMostrar.setText("MOSTRAR");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });

        txtBuscado.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N

        btnCancel.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnCancel.setText("CANCELAR");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        cmbBuscador.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cmbBuscador.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione", "Cliente", "Vehiculo" }));
        cmbBuscador.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbBuscadorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(155, 155, 155)
                        .addComponent(jLabel6))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnMostrar, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                            .addComponent(lblBuscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(43, 43, 43)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtBuscado)
                            .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(60, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(cmbBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(142, 142, 142))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(jLabel6)
                .addGap(38, 38, 38)
                .addComponent(cmbBuscador, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBuscado, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnMostrar))
                .addGap(62, 62, 62))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 433, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 334, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        String clave = txtBuscado.getText();
        JFrame ventana = null;
        
        if(cmbBuscador.getSelectedIndex() == 0){
            btnMostrar.setEnabled(false);
        }else
            if(cmbBuscador.getSelectedIndex() == 1){
                try{
                    Cliente cliente = baseDeDatos.getClientes().recuperarCliente(clave);
                    ventana = new VistaEliminar(cliente);
                    ventana.setVisible(true);
                    ventana.setLocationRelativeTo(null);
                } catch (ClienteNoEncontradoException ex) {
                        JOptionPane.showMessageDialog(this, ex.toString(), "No hay coincidencia", JOptionPane.ERROR_MESSAGE);
            }
            }else 
                if(cmbBuscador.getSelectedIndex() == 2){
                    try{
                        Vehiculo vehiculo = baseDeDatos.getParque().consultarVehiculo(clave);
                        ventana = new VistaEliminar(vehiculo);
                        ventana.setVisible(true);
                        ventana.setLocationRelativeTo(null);
            
                    } catch (VehiculoInexistenteException e){
                        JOptionPane.showMessageDialog(this, e.toString(), "No hay coincidencia", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                }
        
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void cmbBuscadorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbBuscadorActionPerformed
        if(cmbBuscador.getSelectedIndex() == 0){
            txtBuscado.setEnabled(false);
            btnMostrar.setEnabled(false);
        }else
        if (cmbBuscador.getSelectedIndex()==1) {
            lblBuscar.setText("Ingrese DNI");
            txtBuscado.setEnabled(true);
            btnMostrar.setEnabled(true);
        }else
        if (cmbBuscador.getSelectedIndex()==2) {
            lblBuscar.setText("Ingrese PATENTE");
            txtBuscado.setEnabled(true);
            btnMostrar.setEnabled(true);
        }
    }//GEN-LAST:event_cmbBuscadorActionPerformed

   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JComboBox<String> cmbBuscador;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblBuscar;
    private javax.swing.JTextField txtBuscado;
    // End of variables declaration//GEN-END:variables
}
