
package parqueautomotor.gui.menuBorrar;


import javax.swing.JOptionPane;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.excepciones.ClienteNoEncontradoException;
import parqueautomotor.logica.excepciones.VehiculoInexistenteException;
import parqueautomotor.logica.parque.Empresa;
import parqueautomotor.logica.parque.Vehiculo;

/**
 *
 * @author AlexDev
 */
public class VistaEliminar extends javax.swing.JFrame {

    private Object eliminando;
    /**
     * Creates new form VistaEliminar
     * @param eliminando
     */
    public VistaEliminar(Object eliminando) {
        this.eliminando = eliminando;
        initComponents();
        mostrarContenido(eliminando);
        txtApellidOmarca.setEditable(false);
        txtDniOPatente.setEditable(false);
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblTitulo = new javax.swing.JLabel();
        lblApellidoOmarca = new javax.swing.JLabel();
        lblDniOPatente = new javax.swing.JLabel();
        txtApellidOmarca = new javax.swing.JTextField();
        txtDniOPatente = new javax.swing.JTextField();
        btnEliminar = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblTitulo.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        lblApellidoOmarca.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        lblDniOPatente.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        txtApellidOmarca.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        txtDniOPatente.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        btnEliminar.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnEliminar.setText("ELIMINAR");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnCancel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        btnCancel.setText("CANCELAR");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(85, 85, 85)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnEliminar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnCancel))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblApellidoOmarca, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblDniOPatente, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(36, 36, 36)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDniOPatente, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtApellidOmarca, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(147, 147, 147)
                        .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(72, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(lblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtApellidOmarca)
                    .addComponent(lblApellidoOmarca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtDniOPatente)
                    .addComponent(lblDniOPatente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 65, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEliminar)
                    .addComponent(btnCancel))
                .addGap(78, 78, 78))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        JOptionPane.showConfirmDialog(null, "Realmente desea eliminar definitivamente este " + eliminando.getClass().getSimpleName().toString(), "Confirmar eliminacion", JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE);
        try{
            if(eliminando instanceof Cliente){
                Empresa.getInstance().getClientes().quitarCliente(((Cliente)eliminando).getDni());
                JOptionPane.showMessageDialog(this, "Cliente Eliminado");
                dispose();
            }else
            if(eliminando instanceof Vehiculo){
                Empresa.getInstance().getParque().quitarVehiculo(((Vehiculo)eliminando).getPatente());
                JOptionPane.showMessageDialog(this, "Vehiculo Eliminado");
                dispose();
            }
            
        } catch (ClienteNoEncontradoException ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "No hay coincidencia", JOptionPane.ERROR_MESSAGE);
                return;
        } catch (VehiculoInexistenteException ex) {
            JOptionPane.showMessageDialog(this, ex.toString(), "No hay coincidencia", JOptionPane.ERROR_MESSAGE);
                return;
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    public void mostrarContenido(Object eliminado){
        if(eliminado instanceof Cliente){
            lblApellidoOmarca.setText("Apellido y nombre");
            lblDniOPatente.setText("DNI");
            txtApellidOmarca.setText(((Cliente) eliminado).getApellido() + ", " + ((Cliente) eliminado).getNombre());
            txtDniOPatente.setText(((Cliente) eliminado).getDni());
        } else if(eliminado instanceof Vehiculo){
            lblApellidoOmarca.setText("Marca");
            txtApellidOmarca.setText(((Vehiculo) eliminado).getMarca());
            lblDniOPatente.setText("Patente");
            txtDniOPatente.setText(((Vehiculo) eliminado).getPatente());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblApellidoOmarca;
    private javax.swing.JLabel lblDniOPatente;
    private javax.swing.JLabel lblTitulo;
    private javax.swing.JTextField txtApellidOmarca;
    private javax.swing.JTextField txtDniOPatente;
    // End of variables declaration//GEN-END:variables
}
