package parqueautomotor.logica.excepciones;

public class VehiculoNoAlquilableException extends Exception{
    public VehiculoNoAlquilableException(String mensaje){
        super(mensaje);
    }
}
