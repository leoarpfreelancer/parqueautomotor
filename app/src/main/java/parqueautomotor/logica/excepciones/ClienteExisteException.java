package parqueautomotor.logica.excepciones;

public class ClienteExisteException extends Exception {
    public ClienteExisteException(String mensaje){
        super(mensaje);
    }
}
