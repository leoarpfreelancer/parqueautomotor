package parqueautomotor.logica.excepciones;

public class RegistroVacioException extends Exception {
    public RegistroVacioException(String mensaje){
        super(mensaje);
    }
}
