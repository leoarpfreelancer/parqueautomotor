package parqueautomotor.logica.excepciones;

public class EdadClienteExteption extends Exception {
    public EdadClienteExteption(String mensaje){
        super(mensaje);
    }
}
