package parqueautomotor.logica.excepciones;

public class VehiculoNoVendibleException extends Exception {
    public VehiculoNoVendibleException(String mensaje){
        super(mensaje);
    }
}
