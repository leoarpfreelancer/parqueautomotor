package parqueautomotor.logica.excepciones;

public class NoHayTarjetasException extends Exception {
    public NoHayTarjetasException(String mensaje){
        super(mensaje);
    }
}
