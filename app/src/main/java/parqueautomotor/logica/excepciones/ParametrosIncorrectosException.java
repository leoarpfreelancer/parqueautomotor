package parqueautomotor.logica.excepciones;

public class ParametrosIncorrectosException extends Exception {
    public ParametrosIncorrectosException(String mensaje){
        super(mensaje);
    }
}
