package parqueautomotor.logica.excepciones;

public class VehiculoExistenteException extends Exception {
    public VehiculoExistenteException(String mensaje){
        super(mensaje);
    }
}
