package parqueautomotor.logica.excepciones;

public class VehiculoNoDisponibleException extends Exception{
    public VehiculoNoDisponibleException(String mensaje){
        super(mensaje);
    }
}
