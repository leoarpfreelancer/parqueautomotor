package parqueautomotor.logica.excepciones;

public class ClienteEdadInsuficienteException extends Exception{
    public ClienteEdadInsuficienteException(String mensaje){
        super(mensaje);
    }
}
