package parqueautomotor.logica.excepciones;

public class VehiculoInexistenteException extends Exception {
    public VehiculoInexistenteException(String mensaje){
        super(mensaje);
    }
}
