package parqueautomotor.logica.parque;

import java.util.ArrayList;
import java.util.List;

import parqueautomotor.logica.excepciones.*;
import parqueautomotor.logica.vehiculoCarga.*;
import parqueautomotor.logica.vehiculoPasajero.*;


public class ParqueAutomotor {
    
    public List<Vehiculo> vehiculos = new ArrayList<>();

    public List<Vehiculo> getListadoVehiculos(){
        return vehiculos;
    }

    public void registrarVehiculo(Vehiculo vehiculo) throws VehiculoExistenteException{
        if (!comprobarRegistro(vehiculo)) {
            vehiculo.setDisponibilidad(true);
            vehiculos.add(vehiculo);
        }else{
            throw new VehiculoExistenteException("Ya existe un vehiculo con esta patente");
        }
    }

    public boolean comprobarRegistro(Vehiculo movil){
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo.equals(movil)) {
                return true;
            }
        }
        return false;
    }

    public List<Vehiculo> listarVehiculosDisponibles(){
        ArrayList<Vehiculo> disponibles = new ArrayList<>();
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo.isDisponibilidad()) {
                disponibles.add(vehiculo);
            }
        }
        return disponibles;
    }


    public void vender(Vehiculo auto) throws VehiculoNoDisponibleException, VehiculoInexistenteException { 
        if (auto instanceof Auto0Km || auto instanceof AutoUsado) {
            for (Vehiculo vehiculo : vehiculos) {
                if (vehiculo.getPatente().equals(auto.getPatente())) {
                    if (vehiculo.isDisponibilidad()) {
                        vehiculo.setDisponibilidad(false);
                        return;
                    }else {
                    throw new VehiculoNoDisponibleException("El vehiculo no esta disponible");
                    }
                }
            }    
        }
        throw new VehiculoInexistenteException("Este vehiculo no existe en nuestro registro");        
    }

    public void reservarVehiculo(Vehiculo movil)  {    
        List<? extends Vehiculo> listaVehiculos = null;
        listaVehiculos = recuperarListadoAlquiler();
    
        for (Vehiculo vehiculo : listaVehiculos) {
            if (vehiculo.getPatente().equals(movil.getPatente())) {
                if (vehiculo.isDisponibilidad() == true) {
                    vehiculo.setDisponibilidad(false);
                    return; 
                }                
            }
        }    
    }

    @SuppressWarnings("unused")
    public double presupuestarVentaAuto(Vehiculo movil) throws ParametrosIncorrectosException{
        List<Vehiculo> listaVehiculos = recuperarListadoVenta();
        
        for (Vehiculo vehiculo : vehiculos) {
            if(vehiculo.equals(movil)){
                if (vehiculo instanceof AutoUsado && movil instanceof AutoUsado) {
                    return ((AutoUsado) vehiculo).calcularPrecioVenta();
                }
                if (vehiculo instanceof Auto0Km && movil instanceof Auto0Km && vehiculo.equals(movil)){
                    return ((Auto0Km) vehiculo).calcularPrecioVenta();    
                }
            }
        } 
        throw new ParametrosIncorrectosException("Este vehiculo no coincide a lo solicitado");

    }
    
    public double presupuestarAlquilerPasajero(Vehiculo movil, int dias, int plazas)throws ParametrosIncorrectosException{        
        if (movil instanceof AutoUsado) {
            return ((AutoUsado) movil).calcularPrecioAlquiler(dias, plazas);
        } else if (movil instanceof Minibus) {
            return ((Minibus) movil).calcularPrecioAlquiler(dias, plazas);
        }            
        throw new ParametrosIncorrectosException("Este vehiculo no coincide a lo solicitado");
    }

    public double presupuestarAlquilerCarga(Vehiculo movil, double km)throws ParametrosIncorrectosException{
        if (movil instanceof Camion) {
            return ((Camion) movil).calcularPrecioAlquiler(km);
        } else if (movil instanceof Camioneta) {
            return ((Camioneta) movil).calcularPrecioAlquiler(km);
        }        
        throw new ParametrosIncorrectosException("Este vehiculo no coincide a lo solicitado");
    }
    
    public boolean comprobarDisponibilidad(Vehiculo vehiculo){        
        for (Vehiculo movil : vehiculos) {
            if (movil.equals(vehiculo)) {
                if (movil.isDisponibilidad()) {
                    return true;
                }
            }
        }
        return false;
    }

    public List<Vehiculo> recuperarListadoVenta(){
        List<Vehiculo> vendibles = new ArrayList<>();
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo instanceof Auto0Km || vehiculo instanceof AutoUsado) {
                if(vehiculo.isDisponibilidad()){
                    vendibles.add(vehiculo);
                }
            }
        }
        return vendibles; 
    }

    public List<Vehiculo> recuperarListadoAlquiler(){
        List<Vehiculo> alquilables = new ArrayList<>();
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo instanceof AutoUsado || vehiculo instanceof Minibus || vehiculo instanceof Camion || vehiculo instanceof Camioneta) {
                if(vehiculo.isDisponibilidad()){
                    alquilables.add(vehiculo);
                }
            }    
        }
        return alquilables; 
    }

    public List<Vehiculo> recuperarListadoAlquilerDePasajeros(){
        List<Vehiculo> alquilablesPasajero = new ArrayList<>();
        for (Vehiculo movil : vehiculos) {
            if (movil instanceof AutoUsado || movil instanceof Minibus){
            alquilablesPasajero.add(movil);
            }
        }
        return alquilablesPasajero;
    }

    public List<Vehiculo> recuperarListadoAlquilerDeCarga(){
        List<Vehiculo> alquilablesCarga = new ArrayList<>();
        for (Vehiculo movil : vehiculos) {
            if (movil instanceof Camion || movil instanceof Camioneta){
                alquilablesCarga.add(movil);
            }
        }
        return alquilablesCarga;
    }
    
    public List<Vehiculo> listadoAutosUsados(){
        List<Vehiculo> moviles = new ArrayList<>();
        for(Vehiculo movil : vehiculos){
            if(movil instanceof AutoUsado){
                if(movil.isDisponibilidad()){
                    moviles.add(movil);
                }
            }
        }
        return moviles;
    }
    
    public List<Vehiculo> listadoMinibuses(){
        List<Vehiculo> moviles = new ArrayList<>();
        for(Vehiculo movil : vehiculos){
            if(movil instanceof Minibus){
                if(movil.isDisponibilidad()){
                    moviles.add(movil);
                }
            }
        }
        return moviles;
    }

    public List<Vehiculo> listadoCamiones(){
        List<Vehiculo> moviles = new ArrayList<>();
        for(Vehiculo movil : vehiculos){
            if(movil instanceof Camion){
                if(movil.isDisponibilidad()){
                    moviles.add(movil);
                }
            }
        }
        return moviles;
    }
    
    public List<Vehiculo> listadoCamionetas(){
        List<Vehiculo> moviles = new ArrayList<>();
        for(Vehiculo movil : vehiculos){
            if(movil instanceof Camioneta){
                if(movil.isDisponibilidad()){
                    moviles.add(movil);
                }
            }
        }
        return moviles;
    }
    
    public List<Vehiculo> listadoAutosNuevos(){
        List<Vehiculo> moviles = new ArrayList<>();
        for(Vehiculo movil : vehiculos){
            if(movil instanceof Auto0Km){
                if(movil.isDisponibilidad()){
                    moviles.add(movil);
                }
            }
        }
        return moviles;
    }
    
    @SuppressWarnings("unchecked")
    public <T extends Vehiculo> List<T> listarTipoVehiculo(Class<T> tipo){
        List<T> moviles = new ArrayList<>();
        if(tipo.equals(Auto0Km.class)){
            moviles = (List<T>) listadoAutosUsados();
        }
        if(tipo.equals(Minibus.class)){
            moviles = (List<T>) listadoMinibuses();
        }
        if(tipo.equals(Camion.class)){
            moviles = (List<T>) listadoCamiones();
        }
        if(tipo.equals(Camioneta.class)){
            moviles = (List<T>) listadoCamionetas();
        }
        return moviles;
    }
    
    public void recibirDevolucionVehiculo(Vehiculo movil){
        for (Vehiculo vehiculo : vehiculos) {
            if (vehiculo.equals(movil)) {
                vehiculo.setDisponibilidad(true);
                return;
            }
        }
    }
    
    public Vehiculo consultarVehiculo(String patente) throws VehiculoInexistenteException{
        for(Vehiculo movil : vehiculos){
            if(movil.getPatente().equals(patente)){
                return movil;
            }
        }
        throw new VehiculoInexistenteException("Esta patente no esta registrada");
    }
    
    public Vehiculo recuperarVehiculo(String patente) throws VehiculoInexistenteException{
        for(Vehiculo movil : vehiculos){
            if(movil.getPatente().equals(patente)){
                Vehiculo vehiculo = movil;
                return vehiculo;
            }
        }
        throw  new VehiculoInexistenteException("Patente no registrada");
    }

    public boolean tieneObjeto(List<Vehiculo> vehiculos, Class<?> tipo ){
        for(Vehiculo movil : vehiculos){
            if(tipo.isInstance(movil)){
                return true;
            }
        }
        return false;
    }
    
    public int obtenerPosicionVehiculo(String patente) throws VehiculoInexistenteException{
        for(Vehiculo vehiculo : vehiculos){
            if(vehiculo.compararPatente(patente));
            return vehiculos.indexOf(vehiculo);
        }
        throw  new VehiculoInexistenteException("Patente no registrada");
    }
    
    
    public void quitarVehiculo(String patente) throws VehiculoInexistenteException{
        for(Vehiculo vehiculo : vehiculos){
            if(vehiculo.compararPatente(patente)){
                vehiculos.remove(vehiculo);
                return;
            }
        }throw new VehiculoInexistenteException("Patente no registrada");
    }
}
