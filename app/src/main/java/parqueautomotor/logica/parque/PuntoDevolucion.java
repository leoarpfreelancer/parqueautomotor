package parqueautomotor.logica.parque;

public enum PuntoDevolucion {
    SEDE(0.0),
    OTRO_PUNTO(0.35);

    private final double cargo;

    PuntoDevolucion(double cargo) {
        this.cargo = cargo;
    }

    public double getCargo() {
        return cargo;
    }
}
