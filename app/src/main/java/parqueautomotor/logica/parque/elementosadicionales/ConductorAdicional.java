package parqueautomotor.logica.parque.elementosadicionales;

public class ConductorAdicional extends CaracteristicaAdicional {
    private static final double costo = 2800;

    public ConductorAdicional() {
        super(costo);
    }

    
    @Override
    public double obtenerValor() {
        return costo;
    }

    
}
