package parqueautomotor.logica.parque.elementosadicionales;

public class SillaParaBebe extends CaracteristicaAdicional{
        private static final double costo = 2500;

    public SillaParaBebe() {
        super(costo);
    }

    @Override
    public double obtenerValor() {
        return costo;
    }

    
}
