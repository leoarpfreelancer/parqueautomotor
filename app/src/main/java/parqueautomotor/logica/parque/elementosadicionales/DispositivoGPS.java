package parqueautomotor.logica.parque.elementosadicionales;

public class DispositivoGPS extends CaracteristicaAdicional {
    private static final double costo = 3000;
    
    public DispositivoGPS() {
        super(costo);
    }


    @Override
    public double obtenerValor() {
        return costo;
    }
    
    

    
}
