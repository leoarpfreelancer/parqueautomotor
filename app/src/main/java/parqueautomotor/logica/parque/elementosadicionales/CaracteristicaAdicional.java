package parqueautomotor.logica.parque.elementosadicionales;

public abstract class CaracteristicaAdicional {
    protected double valor;
    
    public CaracteristicaAdicional(double valor) {
        this.valor = valor;
    }

    public abstract double obtenerValor();

    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }
    
}
