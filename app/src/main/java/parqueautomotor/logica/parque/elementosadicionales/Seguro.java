package parqueautomotor.logica.parque.elementosadicionales;

public class Seguro extends CaracteristicaAdicional{
    private static final double costo = 1000;
    public Seguro(){
        super(costo);
    }

    @Override
    public double obtenerValor() {
        return costo;
    }
}
