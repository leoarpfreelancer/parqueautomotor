package parqueautomotor.logica.parque;

import java.time.LocalDate;

import parqueautomotor.logica.cliente.*;
import parqueautomotor.logica.excepciones.ClienteEdadInsuficienteException;
import parqueautomotor.logica.excepciones.ParametrosIncorrectosException;
import parqueautomotor.logica.vehiculoCarga.VehiculoDeCarga;
import parqueautomotor.logica.vehiculoPasajero.*;
import parqueautomotor.logica.parque.registroOperaciones.*;



public class Empresa {
    private static final Empresa instancia = new Empresa();
    private ParqueAutomotor parque = new ParqueAutomotor();
    private RegistroCliente clientes = new RegistroCliente();
    private RegistroVentas ventas = new RegistroVentas();
    private RegistroReserva reservas = new RegistroReserva();
    private PuntoDevolucion puntoDevolucion;

    private Empresa(){}

    public static Empresa getInstance() {
        return instancia;
    }

    public ParqueAutomotor getParque() {
        return parque;
    }

    public RegistroCliente getClientes() {
        return clientes;
    }

    public RegistroVentas getVentas() {
        return ventas;
    }

    public RegistroReserva getReservas() {
        return reservas;
    }

    public PuntoDevolucion getPunto(){
        return puntoDevolucion;
    }

   

    public void realizarVenta(Cliente cliente, Vehiculo auto, LocalDate fecha) throws Exception{
        double precioVenta;
        precioVenta = parque.presupuestarVentaAuto(auto);
        parque.vender(auto);
        Vendido venta = new Vendido(cliente, auto, precioVenta, fecha);
        RegistroVentas.registrarVenta(venta);
    }

    

    public double cotizarCostoAlquilerPasajero(Reserva reserva, int dias, int plazas) throws ParametrosIncorrectosException{
        double precioFinal=0;
        if ( reserva.getVehiculo() instanceof AutoUsado || reserva.getVehiculo() instanceof Minibus) {
            precioFinal=parque.presupuestarAlquilerPasajero(reserva.getVehiculo(), dias, plazas);
        }else{
            throw new ParametrosIncorrectosException("Datos invalidos");
        }
        

        return precioFinal;
    }

    private boolean solicitarReservaDeVehiculo(Cliente cliente, Vehiculo vehiculo, LocalDate fecha){
        if (clientes.clienteExiste(cliente.getDni())) {
            if (parque.comprobarDisponibilidad(vehiculo)) {
                if (fecha.isAfter(LocalDate.now())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void reservarVehiculo(Cliente cliente, Vehiculo movil, LocalDate fecha, boolean seguro, boolean silla, boolean gps, boolean chofer) throws ClienteEdadInsuficienteException, ParametrosIncorrectosException  {
        
        if(solicitarReservaDeVehiculo(cliente, movil, fecha)){
            Reserva reserva = new Reserva(cliente, movil, fecha, seguro);
            reserva.solicitarExtras(silla, chofer, gps);
            reservas.registrarReserva(reserva);
            parque.reservarVehiculo(movil);
        }    
    }

    public void solicitarCaracteristicaAdicional(Reserva reservado, boolean silla, boolean gps, boolean chofer){
        reservado.solicitarExtras(silla, chofer, gps);
    }

    

    public double obtenerPrecioFinalAlquiler(Reserva reservado, int dias, int plazaOkm, PuntoDevolucion punto) throws ParametrosIncorrectosException{
        double presupuesto = 0;
        for (Reserva reserva : reservas.listarReservaciones()) {
            if (reserva.equals(reservado)) {
                if (reserva.getVehiculo() instanceof VehiculoDePasajero) {
                    presupuesto = parque.presupuestarAlquilerPasajero(reserva.getVehiculo(), dias, plazaOkm);
                    presupuesto += reserva.calcularValorDeExtras(dias);
                }else if (reserva.getVehiculo() instanceof VehiculoDeCarga) {
                    presupuesto = parque.presupuestarAlquilerCarga(reserva.getVehiculo(), plazaOkm);
                    presupuesto += reserva.calcularValorDeExtras(dias);
                }                
            }
        }
        presupuesto += devolucionVehiculo(punto);
        return presupuesto;
    }
    

    public double cotizarCuotasAlquiler(Reserva alquilado, int dias, int kmOplaza, PuntoDevolucion punto) throws ParametrosIncorrectosException{
        double seisCuotas = 0;
        seisCuotas = obtenerPrecioFinalAlquiler(alquilado, dias, kmOplaza, punto) / 6;
        return seisCuotas;
    }

    public double devolucionVehiculo(PuntoDevolucion punto){
        double cargoExtra;
        cargoExtra = punto.getCargo();
        return cargoExtra;
    }

    public void completarAlquiler(Reserva alquilado){
        parque.recibirDevolucionVehiculo(alquilado.getVehiculo());
        alquilado.setEstado(false);
    }
}
