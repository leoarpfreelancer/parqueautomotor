package parqueautomotor.logica.parque;

public abstract class Vehiculo {
    protected String marca;
    protected String patente;
    protected boolean disponibilidad;

    public Vehiculo(String marca, String patente){
        this.marca = marca;
        this.patente = patente;
        this.disponibilidad = true;
    }

    public void setMarca( String marca){
        this.marca = marca;
    }
    public String getMarca(){
        return marca;
    }

    public void setPatente(String patente){
        this.patente = patente;
    }
    public String getPatente(){
        return patente;
    }   

    public boolean isDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(boolean disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    @Override
    public boolean equals(Object obj) {
        Vehiculo auto = (Vehiculo) obj;
        return this.getPatente().equals(auto.getPatente());
    }
    
    public boolean compararPatente(String patente){
        return this.getPatente().equals(patente);
    }
}
