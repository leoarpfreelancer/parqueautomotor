package parqueautomotor.logica.parque;

import java.util.ArrayList;
import java.util.List;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.excepciones.ClienteExisteException;
import parqueautomotor.logica.excepciones.ClienteNoEncontradoException;

public class RegistroCliente {
    private List<Cliente> registroClientes= new ArrayList<>();


    public List<Cliente> getRegistroClientes() {
        return registroClientes;
    }

    public Cliente verFichaCliente(String dni)throws ClienteNoEncontradoException{
        for (Cliente persona : registroClientes) {
            if(persona.getDni().equals(dni)){
                return persona;
            }
        }throw new ClienteNoEncontradoException("No se pudo encontrar el cliente relacionado al dni dado");
    }

    public boolean clienteExiste(String documento){
        for (Cliente cliente : registroClientes) {
            if (cliente.compararDni(documento)) {
                return true;
            }
        }
        return false;        
    }

    public void registrarCliente(Cliente cliente) throws ClienteExisteException{
        if (!clienteExiste(cliente.getDni())) {
            registroClientes.add(cliente);
        } else{
            throw new ClienteExisteException("Ya se ha registrado este cliente");
        }        
    }

    public void ordenarRegistro(){
        registroClientes.sort((c1, c2) -> c1.compareTo(c2));
    }

    public void quitarCliente(String dni) throws ClienteNoEncontradoException{
        for(Cliente persona : registroClientes){
            if(persona.compararDni(dni)){
                registroClientes.remove(persona);
            }
        }
        throw new ClienteNoEncontradoException("No hay registro vinculado a este dni");
    }
    
    public Cliente recuperarCliente(String dni) throws ClienteNoEncontradoException{
        for(Cliente cliente : registroClientes){
            if(cliente.compararDni(dni)){
                Cliente buscado = cliente;
                return buscado;
            }
        }
        throw new ClienteNoEncontradoException("No existe");
    }

    @Override
    public String toString() {
        
        return registroClientes.toString();
    }
}
