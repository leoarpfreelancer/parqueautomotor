package parqueautomotor.logica.parque.registroOperaciones;

import java.util.ArrayList;
import java.util.List;

import parqueautomotor.logica.excepciones.RegistroVacioException;

public class RegistroVentas {
    private static List<Vendido> registroVentas= new ArrayList<>();

    public List<Vendido> getRegistroventas() {
        return registroVentas;
    }

    public static void registrarVenta(Vendido vendido){
        registroVentas.add(vendido);
    }

    public void mostrarRegistros() throws RegistroVacioException{
        if (!registroVentas.isEmpty()) {
            for (Vendido vendido : registroVentas) {
                System.out.println(vendido.toString());
            }
        }else throw new RegistroVacioException("No hay ventas registradas");        
    }
}
