package parqueautomotor.logica.parque.registroOperaciones;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.parque.Vehiculo;

public class RegistroReserva {
    private List<Reserva> registroDeReservas = new ArrayList<>();

    public RegistroReserva() {        
    }

    public List<Reserva> listarReservaciones() {
        return registroDeReservas;
    }

    public void registrarReserva(Reserva alquilado){
        registroDeReservas.add(alquilado);
    }

    public void ordenarPorFecha(){}

    public List<Reserva> listarReservasPorCliente(Cliente cliente){
        List<Reserva> listaReservas = new ArrayList<>();
        for (Reserva reserva : registroDeReservas) {
            if (reserva.getCliente().equals(cliente)) {
                listaReservas.add(reserva);
            }
        }
        return listaReservas;
    }

    public List<Reserva> listarReservacionesDeVehiculo(Vehiculo vehiculo){
        List<Reserva> historialVehiculo = new ArrayList<>();
        for (Reserva reserva : registroDeReservas) {
            if (reserva.getVehiculo().equals(vehiculo)) {
                historialVehiculo.add(reserva);
            }
        }
        return historialVehiculo;
    }

    public List<Reserva> listarReservasEnFecha(LocalDate fecha){
        List<Reserva> reservasFecha = new ArrayList<>();
        for (Reserva reserva : registroDeReservas) {
            if (reserva.getFechaDeReserva().isEqual(fecha)) {
                reservasFecha.add(reserva);
            }
        }
        return reservasFecha;
    }

    public List<Reserva> listarReservasEnPeriodo(LocalDate fechaInicial, LocalDate fechaFinal){
        List<Reserva> reservasFecha = new ArrayList<>();
        for (Reserva reserva : registroDeReservas) {
            if (reserva.getFechaDeReserva().isAfter(fechaInicial) && reserva.getFechaDeReserva().isBefore(fechaFinal)) {
                reservasFecha.add(reserva);
            }
        }
        return reservasFecha;
    }

    

    
}
