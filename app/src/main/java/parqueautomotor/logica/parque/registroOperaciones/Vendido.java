package parqueautomotor.logica.parque.registroOperaciones;

import java.time.LocalDate;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.parque.Vehiculo;

public class Vendido {
    private Cliente comprador;
    private Vehiculo auto;
    private LocalDate fechaCompra;
    private double preciofinal;
    
    public Vendido(Cliente comprador, Vehiculo auto, double preciofinal, LocalDate fechaCompra) {
        this.comprador = comprador;
        this.auto = auto;
        this.preciofinal = preciofinal;
        this.fechaCompra = fechaCompra;
    }

    public Cliente getComprador() {
        return comprador;
    }
    public void setComprador(Cliente comprador) {
        this.comprador = comprador;
    }

    public Vehiculo getAuto() {
        return auto;
    }
    public void setAuto(Vehiculo auto) {
        this.auto = auto;
    }

    public LocalDate getFechaCompra() {
        return fechaCompra;
    }
    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public double getPreciofinal() {
        return preciofinal;
    }
    public void setPreciofinal(double preciofinal) {
        this.preciofinal = preciofinal;
    }
   
    
}
