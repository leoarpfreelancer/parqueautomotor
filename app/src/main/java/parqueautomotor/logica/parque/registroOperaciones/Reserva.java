package parqueautomotor.logica.parque.registroOperaciones;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import parqueautomotor.logica.cliente.Cliente;
import parqueautomotor.logica.excepciones.ClienteEdadInsuficienteException;
import parqueautomotor.logica.excepciones.ParametrosIncorrectosException;
import parqueautomotor.logica.parque.*;
import parqueautomotor.logica.parque.elementosadicionales.*;

public class Reserva {
    private Cliente cliente;
    private Vehiculo vehiculo;
    private LocalDate fechaDeReserva;
    private List<CaracteristicaAdicional> extras  = new ArrayList<>();
    private boolean activo;

    public Reserva(Cliente cliente, Vehiculo vehiculo, LocalDate fechaDeReserva, boolean seguro) throws ClienteEdadInsuficienteException, ParametrosIncorrectosException {
        if (cliente.getTarjeta() == null) {
            throw new ParametrosIncorrectosException("Debe poseer registrada una tarjeta de credito");
        }
        if (seguro) {
            if(cliente.mayorDeEdad()){
                this.cliente = cliente;
                this.vehiculo = vehiculo;
                this.fechaDeReserva = fechaDeReserva;
                this.extras.add(new Seguro());
                this.activo = true;
            }else 
                throw new ClienteEdadInsuficienteException("Debe ser mayor de 21 años para contratar seguro");
        }else{
            this.cliente = cliente;
            this.vehiculo = vehiculo;
            this.fechaDeReserva = fechaDeReserva;
            this.activo = true;
        }        
    }

    public boolean getEstado(){
        return activo;
    }
    public void setEstado(boolean activo){
        this.activo = activo;
    }

    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }
    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public LocalDate getFechaDeReserva() {
        return fechaDeReserva;
    }
    public void setFechaDeReserva(LocalDate fechaDeReserva) {
        this.fechaDeReserva = fechaDeReserva;
    }

    public List<CaracteristicaAdicional> getExtras() {
        return extras;
    }

    
    public void agregarExtra(CaracteristicaAdicional adicional){
        extras.add(adicional);
    }
    
    public void quitarExtra(CaracteristicaAdicional extra){
        extras.remove(extra);
    }


    public double calcularValorDeExtras(int dias){
        double costoAdicional = 0;
        for (CaracteristicaAdicional adicional : extras) {
            if (adicional instanceof Seguro) {
                costoAdicional+=adicional.obtenerValor();
            }else {
                costoAdicional += adicional.obtenerValor() * dias;
            }
        }
        return costoAdicional;
    }

    public void solicitarExtras(boolean silla, boolean chofer, boolean gps){
        if (silla) {
            extras.add(new SillaParaBebe());
        }
        if (gps) {
            extras.add(new DispositivoGPS());
        }
        if (chofer) {
            extras.add(new ConductorAdicional());
        }
    }

    public void contratarSeguro(boolean seguro, Cliente cliente) throws ClienteEdadInsuficienteException{
        if (seguro) {
            if (cliente.mayorDeEdad()) {
                extras.add(new Seguro());
            }throw new ClienteEdadInsuficienteException("Debe ser mayor a 21 años para contratar seguro");
        }
    }
    
    public boolean saberSiTieneSilla(){
        for(CaracteristicaAdicional extra : extras){
            if(extra instanceof SillaParaBebe){
                return true;
            }
        }
        return false;
    }

    public boolean saberSiTieneGPS(){
        for(CaracteristicaAdicional extra : extras){
            if(extra instanceof DispositivoGPS){
                return true;
            }
        }
        return false;
    }
    
    public boolean saberSiTieneChofer(){
        for(CaracteristicaAdicional extra : extras){
            if(extra instanceof ConductorAdicional){
                return true;
            }
        }
        return false;
    }
    
    public boolean saberSiTieneSeguro(){
        for(CaracteristicaAdicional extra : extras){
            if(extra instanceof Seguro){
                return true;
            }
        }
        return false;
    }
}
