package parqueautomotor.logica.vehiculoPasajero;

import java.util.ArrayList;

import parqueautomotor.logica.auxiliares.PrecioVentaAuto;
import parqueautomotor.logica.vehiculoPasajero.componentes.ComponenteExtra;

public class Auto0Km extends Auto {
    private ArrayList<ComponenteExtra> componentes;

    public Auto0Km(String marca, String patente, double precio_base) {
        super(marca, patente, precio_base);
        this.componentes = new ArrayList<ComponenteExtra>();
    }

    public void agregarComponente(ComponenteExtra componente) {
        componentes.add(componente);
    }

    public void quitarComponente(ComponenteExtra componente){
        componentes.remove(componente);
    }

    public double obtenerValorComponentes() {
        double valorComponente = 0;
        for (ComponenteExtra componente : componentes) {
            valorComponente += componente.obtenerValor();
        }
        return valorComponente;
    }

    public double valorComponentes() {
        double precioComponentes = 0;
        precioComponentes = this.obtenerValorComponentes() * this.precio_base;
        return precioComponentes;
    }


    public double calcularPrecioVenta() {
        double precio = 0;
        precio = this.precio_base + this.valorComponentes();
        precio = PrecioVentaAuto.calcularPrecioVentaNuevo(precio);
        return precio;
    }

    public ArrayList<ComponenteExtra> getComponentes() {
        return componentes;
    }

    public void setComponentes(ArrayList<ComponenteExtra> componentes) {
        this.componentes = componentes;
    }


    
}
