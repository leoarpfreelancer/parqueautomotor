package parqueautomotor.logica.vehiculoPasajero.componentes;


public class LevantaCristalElectrico extends ComponenteExtra {
    private double valor = LEVANTACRISTALELECTRICO;

    public LevantaCristalElectrico() {
    }

    @Override
    public double obtenerValor(){
        return valor;
    }
    
}
