package parqueautomotor.logica.vehiculoPasajero.componentes;

public class Alarma extends ComponenteExtra {

    private double valor = ALARMA;

    public Alarma() {
    }
    
    @Override
    public double obtenerValor(){
        return valor;
    }
}
