package parqueautomotor.logica.vehiculoPasajero.componentes;

public abstract class ComponenteExtra {
    public final static double ALARMA = 0.01;
    public final static double LEVANTACRISTALELECTRICO = 0.01;
    public final static double AIREACONDICIONADO = 0.01;
    
    public abstract double obtenerValor();

    @Override
    public String toString(){
        return this.getClass().getSimpleName();
    }

}
