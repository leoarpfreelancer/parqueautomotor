package parqueautomotor.logica.vehiculoPasajero.componentes;


public class AireAcondicionado extends ComponenteExtra {
    private double valor = AIREACONDICIONADO;
    
    public AireAcondicionado(){
    }

    
    @Override
    public double obtenerValor(){
        return valor;
    }
}
