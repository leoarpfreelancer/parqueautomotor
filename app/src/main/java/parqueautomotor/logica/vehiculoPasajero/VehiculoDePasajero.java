package parqueautomotor.logica.vehiculoPasajero;

import parqueautomotor.logica.parque.Vehiculo;

public abstract class VehiculoDePasajero extends Vehiculo {

    public VehiculoDePasajero(String marca, String patente) {
        super(marca, patente);
    }

}
