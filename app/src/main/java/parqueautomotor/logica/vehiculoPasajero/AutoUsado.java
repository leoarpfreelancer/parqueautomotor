package parqueautomotor.logica.vehiculoPasajero;

import parqueautomotor.logica.auxiliares.PrecioAlquilerPasajero;
import parqueautomotor.logica.auxiliares.PrecioVentaAuto;

public class AutoUsado extends Auto{

    public AutoUsado(String marca, String patente, double precio_base) {
        super(marca, patente, precio_base);
    }
    
    public double calcularPrecioVenta() {
        double precio = PrecioVentaAuto.calcularPrecioVentaUsado(precio_base);
        return precio;
    }

    public double calcularPrecioAlquiler(int dias, int plazas){
        return PrecioAlquilerPasajero.calcularPrecioAlquilerAuto(dias, plazas);
    }

    @Override
    public String toString() {
        return "Auto Usado [marca: " + marca + ", patente: " +patente+ ", precio venta:" + calcularPrecioVenta() + ", disponible: "+isDisponibilidad()+"]";
    }
}
