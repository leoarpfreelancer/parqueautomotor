package parqueautomotor.logica.vehiculoPasajero;

public abstract class Auto extends VehiculoDePasajero {
    protected double precio_base;

    public Auto(String marca, String patente, double precio_base) {
        super(marca, patente);
        this.precio_base = precio_base;
    }

}
