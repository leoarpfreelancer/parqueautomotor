package parqueautomotor.logica.vehiculoPasajero;

import parqueautomotor.logica.auxiliares.PrecioAlquilerPasajero;

public class Minibus extends VehiculoDePasajero{

    public Minibus(String marca, String patente) {
        super(marca, patente);
    }

    public double calcularPrecioAlquiler(int dias, int plazas){
        return PrecioAlquilerPasajero.calcularPrecioAlquilerMinibus(dias, plazas);
    }

    @Override
    public String toString() {
        return "Minibus [marca: " + marca + ", patente: " + patente + ", disponible: "+ isDisponibilidad()+"]";
    }
}
