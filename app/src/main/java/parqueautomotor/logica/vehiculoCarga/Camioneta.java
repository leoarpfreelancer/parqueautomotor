package parqueautomotor.logica.vehiculoCarga;

import parqueautomotor.logica.auxiliares.PrecioAlquilerCarga;

public class Camioneta extends VehiculoDeCarga{
    
    public Camioneta(String marca, String patente, double kilometraje) {
        super(marca, patente, kilometraje);
    }


    @Override
    public double calcularPrecioAlquiler(double kmRecorrido){
        return PrecioAlquilerCarga.calcularPrecioAlquilerCamioneta(kmRecorrido);
    }

    @Override
    public String toString() {
        return "Camioneta [marca: " + marca + ", patente: " +patente+ ", disponible: "+isDisponibilidad()+"]";
    }
}
