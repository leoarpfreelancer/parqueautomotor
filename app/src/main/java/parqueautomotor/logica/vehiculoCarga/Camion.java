package parqueautomotor.logica.vehiculoCarga;

import parqueautomotor.logica.auxiliares.PrecioAlquilerCarga;

public class Camion extends VehiculoDeCarga{

    public Camion(String marca, String patente, double kilometraje) {
        super(marca, patente, kilometraje);
    }


    @Override
    public double calcularPrecioAlquiler(double kmRecorrido){
        return PrecioAlquilerCarga.calcularPrecioAlquilerCamion(kmRecorrido);
    }


    @Override
    public String toString() {
        return "Camion [marca: " + marca + ", patente: " +patente+ ", disponible: "+isDisponibilidad()+"]";
    }
    
    
}
