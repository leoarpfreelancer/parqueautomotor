package parqueautomotor.logica.vehiculoCarga;

import parqueautomotor.logica.parque.Vehiculo;

public abstract class VehiculoDeCarga extends Vehiculo {
    private double kilometraje;

    public VehiculoDeCarga(String marca, String patente, double kilometraje) {
        super(marca, patente);
        this.kilometraje = kilometraje;
    }
    
    public void setKilometraje(double kilometraje){
        this.kilometraje = kilometraje;
    }
    public double getKilometraje(){
        return kilometraje;
    }

    public abstract double calcularPrecioAlquiler(double kmRecorrido);

}
