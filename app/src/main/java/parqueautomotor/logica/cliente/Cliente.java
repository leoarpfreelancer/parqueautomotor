package parqueautomotor.logica.cliente;

import java.time.LocalDate;
import java.time.Period;


public class Cliente implements Comparable<Cliente>{
    private String apellido;
    private String nombre;
    private String dni;
    private LocalDate fechaNac;
    private String email;
    private String telefono;
    private TarjetaCredito tarjeta;

    public Cliente(String apellido, String nombre, String dni, LocalDate fechaNac, String email, String telefono, TarjetaCredito tarjeta){
        this.apellido = apellido;
        this.nombre = nombre;
        this.dni = dni;
        this.fechaNac = fechaNac;
        this.email = email;
        this.telefono = telefono;
        this.tarjeta = tarjeta;            
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public LocalDate getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(LocalDate fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public TarjetaCredito getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(TarjetaCredito tarjeta) {
        this.tarjeta = tarjeta;
    }


    public int edad(){
        Period periodo = Period.between(fechaNac, LocalDate.now());
        return periodo.getYears();
    }

    public boolean mayorDeEdad(){
        return edad()>= 21;
    }

    @Override
    public int compareTo(Cliente o) {
        int salida;
        if (o.getApellido().compareToIgnoreCase(this.apellido) == 0) {
            if(o.getNombre().compareToIgnoreCase(this.nombre) > 0){
                salida = -1;
            } else if (o.getApellido().compareToIgnoreCase(this.apellido) < 0) {
                salida = 1;
            } else {
                salida = 0;
            }
        } else if (o.getApellido().compareToIgnoreCase(this.apellido) > 0) {
            salida = -1;
        } else{
            salida = 1;
        }
        return salida;
    }

    @Override
    public boolean equals(Object obj) {
        Cliente comparar = (Cliente) obj;
        return this.getDni().equals(comparar.getDni());
    }
    
    public boolean compararDni(String documento) {
        return dni.equals(documento);
    }

    @Override
    public String toString() {
        return "Cliente [apellido=" + apellido + ", nombre=" + nombre + ", dni=" + dni + ", fechaNac=" + fechaNac
                + ", email=" + email + ", telefono=" + telefono + ", tarjeta=" + tarjeta + "]";
    }

    

}
