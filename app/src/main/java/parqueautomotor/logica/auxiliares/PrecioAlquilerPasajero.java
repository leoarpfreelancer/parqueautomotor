package parqueautomotor.logica.auxiliares;

public class PrecioAlquilerPasajero {
    private final static double SEGURO_AUTO = 50;
    private final static double SEGURO_MINIBUS = 250;

    private final static int ALQUILERXDIAS = 50;


    public static double calcularPrecioAlquilerAuto(int dias, int plaza) {
        double precio = (dias*ALQUILERXDIAS) + (SEGURO_AUTO * plaza) ;
        return precio;
    }

    public static double calcularPrecioAlquilerMinibus(int dias, int plazas) {
        double precio = (dias*ALQUILERXDIAS) + (SEGURO_MINIBUS * plazas) ;
        return precio;
    }
}
