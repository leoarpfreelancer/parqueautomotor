package parqueautomotor.logica.auxiliares;


public class PrecioAlquilerCarga{
    private final static double BASE = 300;
    private final static double KM_BASE = 50;
    private final static double EXTRA = 200;
    private final static double ADICIONALPORKM = 20;



    
    public static double calcularPrecioAlquilerCamion(double km) {
        double precio;
        if (km<=KM_BASE) {
            precio = BASE;
            return precio;
        } else
            precio = BASE + EXTRA;
            return precio;
    }

    public static double calcularPrecioAlquilerCamioneta(double km) {
        double precio;
        if (km <= KM_BASE) {
            precio = BASE;
            return precio;
        }else 
            precio = ADICIONALPORKM*KM_BASE;
            return precio;
    }
}
