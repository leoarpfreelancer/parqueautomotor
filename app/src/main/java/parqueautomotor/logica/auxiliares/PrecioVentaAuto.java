package parqueautomotor.logica.auxiliares;

public class PrecioVentaAuto {
    private final static double UTILIDAD_USADO = 0.35;
    public final static double UTILIDAD_NUEVO = 0.5;


    public static double calcularPrecioVentaUsado(double precio_base) {
        double precio = precio_base + (UTILIDAD_USADO*precio_base) ;
        return precio;
    }

    public static double calcularPrecioVentaNuevo(double precio_base){
        double precio = precio_base + (UTILIDAD_NUEVO * precio_base);
        return precio;
    }
}
